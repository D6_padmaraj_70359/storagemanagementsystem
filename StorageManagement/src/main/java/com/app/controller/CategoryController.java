package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.payload.ApiResponse;
import com.app.payload.CategoryDTO;
import com.app.payload.ProductDTO;
import com.app.service.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {
@Autowired 
private CategoryService categoryservice;

@PostMapping("/addcategory")
private ResponseEntity<?> addcategory(@RequestBody CategoryDTO newcategory){
	String message=categoryservice.addcategory(newcategory);
	return new ResponseEntity(new ApiResponse(message,true),HttpStatus.OK);
}

@DeleteMapping("/deleteCategory/{cid}")
public ResponseEntity<?>deleteCategory(@PathVariable int cid){
	String message=this.categoryservice.removecategory(cid);
	return new ResponseEntity(new ApiResponse(message,true),HttpStatus.ACCEPTED);
}

@GetMapping("/getproductsbyCategoryid/{cid}")
public ResponseEntity<List<ProductDTO>>getallproductbycategoryid(@PathVariable int cid){
	System.out.println("inside productsbycategoryid");
	List<ProductDTO>allproductsofcid=this.categoryservice.findallproductsofcid(cid);
	return new ResponseEntity(allproductsofcid,HttpStatus.FOUND);
}

}
