package com.app.controller;

import java.util.List;

import org.aspectj.weaver.NewConstructorTypeMunger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Exceptions.ResourceNotFoundException;
import com.app.payload.AddproductDTO;
import com.app.payload.ApiResponse;
import com.app.payload.ProductDTO;
import com.app.payload.UpdateProductDTO;
import com.app.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {
@Autowired
private ProductService productservice;

@PostMapping("/addproduct/{cid}")
private ResponseEntity<?> addproduct(@RequestBody AddproductDTO newproduct,@PathVariable ("cid")int cid){
	System.out.println("inside addproduct method");
	//String name=newproduct.getPname();
	//System.out.println(name);
	//int cid=newproduct.getCid();
	String message=productservice.addproduct(newproduct,cid);
	return new ResponseEntity(new ApiResponse(message,true),HttpStatus.OK);
}

@DeleteMapping("/deleteproduct/{pid}")
private ResponseEntity<?>deleteproduct(@PathVariable("pid") int pid){
	System.out.println("in the deleteproduct method");
	String message=this.productservice.removeproduct(pid);
	return new ResponseEntity(new ApiResponse(message, true),HttpStatus.ACCEPTED);
}

@PutMapping("/updateproduct/{pid}")
public ResponseEntity<?>updateproduct(@PathVariable int pid,@RequestBody UpdateProductDTO  updateproduct){
	String message=this.productservice.editproduct(pid,updateproduct);
	return new ResponseEntity(new ApiResponse(message,true),HttpStatus.CREATED);
}

@GetMapping("/getproduct")
public ResponseEntity<List<ProductDTO>>viewallproducts(){
	System.out.println("inside the getproductmethod");
	List<ProductDTO>allproducts=this.productservice.getAllProducts();
	return new ResponseEntity(allproducts,HttpStatus.OK);
}

@GetMapping("/getproductbyid/{pid}")
public ResponseEntity<ProductDTO>getproductbyid(@PathVariable int pid){
	System.out.println("get product by id");
	ProductDTO product=this.productservice.findproduct(pid);
	return new ResponseEntity(product,HttpStatus.FOUND);
	
}
}
