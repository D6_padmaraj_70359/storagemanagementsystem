package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.payload.ApiResponse;
import com.app.payload.UserRegisterDTO;
import com.app.service.UserService;

@RestController
@RequestMapping("/")
public class UserController {
@Autowired
private UserService userservice;

@GetMapping("/dummy")
private ResponseEntity<?> dummy(){
	System.out.println("inside dummy method");
	return new ResponseEntity(new ApiResponse("inside dummy method",true),HttpStatus.OK);
	
}

@PostMapping("/register")
private ResponseEntity<?> registeruser(@RequestBody UserRegisterDTO newuser){
	String message=userservice.addnewuser(newuser);
	return new ResponseEntity(new ApiResponse(message,true),HttpStatus.CREATED);
	
}



}
