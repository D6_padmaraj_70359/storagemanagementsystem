package com.app.payload;

import com.app.pojo.Role;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterDTO {
	
	private String name;
	private String password;
	private String email;
	private Role userrole;
}
