package com.app.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Category {
public Category(int cid, String cName, String description) {
		this.cid=cid;
		this.cName=cName;
		this.description=description;
	}

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer cid;
private String cName;
private String description;

@OneToMany(mappedBy = "pcategory",fetch = FetchType.EAGER,
cascade = CascadeType.ALL,orphanRemoval = true)
List<Product>product=new ArrayList<>();

}
