package com.app.pojo;

import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Product {
//public Product(int pid2, String pName2, Long volume2, Optional<Category> existingcategory) {
//		this.pid=pid2;
//		this.pName=pName2;
//		this.volume=volume2;
//		this.pcategory=existingcategory;
//	}

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer pid;
private String pName;
private Long volume;

@ManyToOne
@JoinColumn(name ="cid",nullable = false)
private Category pcategory; 

}
