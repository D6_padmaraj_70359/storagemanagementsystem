package com.app.service;

import java.util.List;

import com.app.payload.CategoryDTO;
import com.app.payload.ProductDTO;

public interface CategoryService {

	String addcategory(CategoryDTO newcategory);

	String removecategory(int cid);

	List<ProductDTO> findallproductsofcid(int cid);

}
