package com.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.Exceptions.ResourceNotFoundException;
import com.app.payload.CategoryDTO;
import com.app.payload.ProductDTO;
import com.app.pojo.Category;
import com.app.pojo.Product;
import com.app.repository.CategoryRepo;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
@Autowired
private CategoryRepo categoryrepo;

//////////////////////////////////////////////////////////////////
	@Override
	public String addcategory(CategoryDTO newcategory) {
		Category category=new Category(0,newcategory.getCName(),newcategory.getDescription());
		Category addcategory=categoryrepo.save(category);
		if(addcategory!=null) {
			return "category added successfully";
		}
		else {
			return "category not added";
		}
		
	}
//////////////////////////////////////////////////////////////
	@Override
	public String removecategory(int cid) {
		// TODO Auto-generated method stub
		String msg;
		Category category=this.categoryrepo.findById(cid).orElseThrow(()->new ResourceNotFoundException("Category Not found"));
		if(category!=null) {
			this.categoryrepo.delete(category);
			msg="Category Deleted Successfully";
		}
		else {
			msg="Category not deleted";
		}
		return msg;
	}
/////////////////////////////////////////////////////////////////
	@Override
	public List<ProductDTO> findallproductsofcid(int cid) {
		Category category=this.categoryrepo.findById(cid).orElseThrow(()->new ResourceNotFoundException("invalid Category"));
		if(category!=null) {
		List<Product>products=category.getProduct().stream().filter((i)->i.getPcategory().getCid()==cid).collect(Collectors.toList());
		products.stream().forEach((i)->System.out.println(i.getPName()));
		List<ProductDTO>productsdto=new ArrayList<>();
		productsdto=productsTodto(products);
		return productsdto;
		
		}
		return null;
	}
	private List<ProductDTO> productsTodto(List<Product> products) {
		// TODO Auto-generated method stub
		List<ProductDTO>listproductdto=new ArrayList<>();
		for(Product p:products) {
			ProductDTO productdto=producttoDTO(p);
			listproductdto.add(productdto);
		}
		return listproductdto;
	}
	private ProductDTO producttoDTO(Product p) {
		// TODO Auto-generated method stub
		ProductDTO prodctdto=new ProductDTO();
		prodctdto.setCid(p.getPcategory().getCid());
		prodctdto.setPid(p.getPid());
		prodctdto.setPname(p.getPName());
		prodctdto.setVolume(p.getVolume());
		return prodctdto;
	}
}
