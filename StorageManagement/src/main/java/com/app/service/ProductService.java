package com.app.service;

import java.util.List;

import com.app.Exceptions.ResourceNotFoundException;
import com.app.payload.AddproductDTO;
import com.app.payload.ProductDTO;
import com.app.payload.UpdateProductDTO;

public interface ProductService {

	String addproduct(AddproductDTO newproduct, int cid);

	String removeproduct(int pid);

	String editproduct(int pid, UpdateProductDTO updateproduct);

	List<ProductDTO> getAllProducts();

	ProductDTO findproduct(int pid);

}
