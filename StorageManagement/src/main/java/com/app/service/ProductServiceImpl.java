package com.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.Exceptions.ResourceNotFoundException;
import com.app.payload.AddproductDTO;
import com.app.payload.ProductDTO;
import com.app.payload.UpdateProductDTO;
import com.app.pojo.Category;
import com.app.pojo.Product;
import com.app.repository.CategoryRepo;
import com.app.repository.ProductRepo;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepo productrepo;
	@Autowired
	private CategoryRepo categoryrepo;
	
	///////////////////////////////////////////////////////////////////
	@Override
	public String addproduct(AddproductDTO newproduct, int cid){
		// TODO Auto-generated method stub
		String msg;
		
		Category existingcategory=this.categoryrepo.findById(cid).orElseThrow(()->new ResourceNotFoundException("Invalid Category"));//.;
		
		if(existingcategory!=null) {
		Product transientproduct=new Product(0,newproduct.getPname(),newproduct.getVolume(),existingcategory);	
		System.out.println(newproduct.getPname());
		productrepo.save(transientproduct);
		msg="product added successfully";
		return msg;
		
		}
		
		
		return "invalid category";
		
	}
	
	//////////////////////////////////////////////////////////////////
	@Override
	public String removeproduct(int pid) {
		// TODO Auto-generated method stub
		Product existingproduct=productrepo.findById(pid).orElseThrow(()->new ResourceNotFoundException("Invalid Product id"));
		
		if(existingproduct!=null) {
			//existingproduct.getPcategory().getProduct().remove(pid);
			//productrepo.delete(existingproduct);
			existingproduct.getPcategory().getProduct().remove(existingproduct);
			productrepo.save(existingproduct);
			//productrepo.delete(product);
			System.out.println("after delete");
			String msg="Product Deleted Sucessfully";
			return msg;
		}
		return "Unable to delete";
	}
	
/////////////////////////////////////////////////////////////////////
	@Override
	public String editproduct(int pid, UpdateProductDTO  updateproduct) {
		// TODO Auto-generated method stub
		Product existingproduct=this.productrepo.findById(pid).orElseThrow(()->new ResourceNotFoundException("invalid Product"));
		//existingproduct.getPcategory().setCid(updateproduct.getCid());
		existingproduct.setPName(updateproduct.getPname());
		existingproduct.setVolume(updateproduct.getVolume());
		this.productrepo.save(existingproduct);
		return "product updated successfully";
	}
////////////////////////////////////////////////////////////////////
	@Override
	public List<ProductDTO> getAllProducts() {
		// TODO Auto-generated method stub
		List<Product>products=this.productrepo.findAll();
		List<ProductDTO> productsdto=new ArrayList<>();
		productsdto=productsTodto(products);
		return productsdto;
	}

	private List<ProductDTO> productsTodto(List<Product> products) {
		List<ProductDTO>productsdto=new ArrayList<ProductDTO>();
		for(Product product:products) {
			ProductDTO prodctdto=prodctToDto(product);
			productsdto.add(prodctdto);
		}
		return productsdto;
	}

	private ProductDTO prodctToDto(Product product) {
		// TODO Auto-generated method stub
		ProductDTO dto=new ProductDTO();
		dto.setCid(product.getPcategory().getCid());
		dto.setPid(product.getPid());
		dto.setPname(product.getPName());
		dto.setVolume(product.getVolume());
		return dto;
	}
//////////////////////////////////////////////////////////////////////////////////////

	@Override
	public ProductDTO findproduct(int pid) {
		ProductDTO newproductdto=new ProductDTO();
		Product product=this.productrepo.findById(pid).orElseThrow(()->new ResourceNotFoundException("Product Not found"));
		newproductdto.setCid(product.getPcategory().getCid());
		newproductdto.setPid(pid);
		newproductdto.setPname(product.getPName());
		newproductdto.setVolume(product.getVolume());
		return newproductdto;
	}
	
}
