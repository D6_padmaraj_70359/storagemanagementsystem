package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.payload.UserRegisterDTO;
import com.app.pojo.Role;
import com.app.pojo.User;
import com.app.repository.UserRepo;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepo userrepo;
	
	@Override
	public String addnewuser(UserRegisterDTO newuser) {
		// TODO Auto-generated method stub
		User user=new User(0,newuser.getName(),newuser.getPassword(),newuser.getEmail(),Role.USER);
		User addeduser=userrepo.save(user);
		if(addeduser!=null) {
			return "user added Successfully";
		}
		else {
			return "user not added";
		}
		
	}
	
	

}
